package com.flopcode.movingthings;

public class MutablePoint {
    public MutableDouble x;
    public MutableDouble y;

    public MutablePoint(String title, double x, double y) {
        this.x = new MutableDouble(title + "X", x);
        this.y = new MutableDouble(title + "Y", y);
    }
}

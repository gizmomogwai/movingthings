package com.flopcode.movingthings.things;

import com.flopcode.movingthings.MutableDouble;
import com.flopcode.movingthings.MutablePoint;

import javax.swing.JComponent;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;

public class Line extends Thing {

    private final MutablePoint from;
    private final MutablePoint to;
    private final MutableDouble duration;
    private final Point2D fromTo;
    private final Point2D toFrom;

    private Point2D last;

    public Line(String name, MutablePoint from, MutablePoint to, double time) {
        super(name);
        this.from = from;
        this.to = to;
        this.duration = new MutableDouble("duration", time);
        this.fromTo = new Point2D.Double(
                (to.x.getValue() - from.x.getValue()),
                (to.y.getValue() - from.y.getValue()));
        this.toFrom = new Point2D.Double(
                (from.x.getValue() - to.x.getValue()),
                (from.y.getValue() - to.y.getValue()));

    }

    @Override
    public Point2D update(double time) {
        double current = time / duration.getValue();
        double lower = Math.floor(current);
        int cyclus = (int) lower;
        if (cyclus % 2 == 0) {
            // on the way from from to to
            last = new Point2D.Double(
                    from.x.getValue() + fromTo.getX() * (current - lower),
                    from.y.getValue() + fromTo.getY() * (current - lower));
        } else {
            // on the way from to to from
            last = new Point2D.Double(
                    to.x.getValue() + toFrom.getX() * (current - lower),
                    to.y.getValue() + toFrom.getY() * (current - lower));
        }
        return last;
    }

    @Override
    public Point2D get() {
        return last;
    }

    @Override
    public JComponent getGui() {
        return null;
    }

    @Override
    public void draw(Graphics2D g2d, Point screenCenter) {
        drawPlanet(g2d, screenCenter.x + (int) get().getX(), screenCenter.y + (int) get().getY(), 10);
    }
}

package com.flopcode.movingthings.things;

import com.flopcode.movingthings.MutableDouble;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RadialGradientPaint;
import java.awt.geom.Point2D;

public abstract class Thing {
    public String name;
    Point2D last;
    float[] fractions = new float[]{0.0f, 1.0f};
    Color[] colors = new Color[]{Color.WHITE, Color.BLACK};

    Thing(String name) {
        this.name = name;
    }

    /**
     * Calc new position
     *
     * @param time since start of universe
     * @return the new position
     */
    public abstract Point2D update(double time);

    /**
     * @return last updated position
     */
    public Point2D get() {
        return last;
    }

    public abstract JComponent getGui();

    void addDoubleEditor(JPanel labels, JPanel editors, MutableDouble v) {
        labels.add(new JLabel(v.getText()));
        JTextField editor = new JTextField("" + v.getValue());
        editor.addActionListener(event -> {
            try {
                v.setValue(Double.parseDouble(editor.getText()));
            } catch (Exception e) {
                System.out.println("e = " + e);
            }
        });
        editors.add(editor);
    }

    public abstract void draw(Graphics2D g2d, Point screenCenter);

    protected void drawPlanet(Graphics2D graphics, int x, int y, int radius) {
        int r2 = radius / 2;
        graphics.setPaint(new RadialGradientPaint(x - r2, y - r2, radius, fractions, colors));
        graphics.fillOval(x - r2, y - r2, radius, radius);
    }

}

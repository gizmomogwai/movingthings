package com.flopcode.movingthings.things;

import com.flopcode.movingthings.MutableDouble;

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.geom.Point2D;

public class Planet extends Thing {
    Thing parent;
    MutableDouble radius;
    MutableDouble period;
    /**
     * phase in degrees
     */
    MutableDouble phase;

    Point2D last;

    public Planet(String name, Thing parent, double radius, double period, double phase) {
        super(name);
        this.parent = parent;
        this.radius = new MutableDouble("radius", radius);
        this.period = new MutableDouble("period", period);
        this.phase = new MutableDouble("phase", phase);
    }

    @Override
    public Point2D update(double time) {
        Point2D center = parent.update(time);
        double alpha = time / period.getValue() * Math.PI * 2 + phase.getValue() / 360 * Math.PI * 2;
        last = new Point2D.Double(
                center.getX() + Math.cos(alpha) * radius.getValue(),
                center.getY() + Math.sin(alpha) * radius.getValue());
        return last;
    }

    @Override
    public Point2D get() {
        return last;
    }

    @Override
    public JComponent getGui() {
        JPanel result = new JPanel(new BorderLayout());
        int elements = 5;
        JPanel labels = new JPanel(new GridLayout(elements, 1));
        JPanel editors = new JPanel(new GridLayout(elements, 1));
        result.add(labels, BorderLayout.WEST);
        result.add(editors, BorderLayout.CENTER);

        addDoubleEditor(labels, editors, radius);
        addDoubleEditor(labels, editors, period);
        addDoubleEditor(labels, editors, phase);
        return result;
    }

    @Override
    public void draw(Graphics2D g2d, Point screenCenter) {
        drawPlanet(g2d, screenCenter.x + (int) get().getX(), screenCenter.y + (int) get().getY(), 20);
    }
}

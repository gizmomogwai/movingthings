package com.flopcode.movingthings.things;

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * Thing at center of the universe
 */
public class Sun extends Thing {
    Point2D last;

    public Sun(String name, int x, int y) {
        super(name);
        last = new Point2D.Double(x, y);
    }

    @Override
    public Point2D update(double time) {
        return last;
    }

    @Override
    public Point2D get() {
        return last;
    }

    @Override
    public JComponent getGui() {
        JPanel result = new JPanel(new BorderLayout());
        return result;
    }

    @Override
    public void draw(Graphics2D g2d, Point screenCenter) {
        drawPlanet(g2d, screenCenter.x, screenCenter.y, 50);
    }

}

package com.flopcode.movingthings;

import com.flopcode.movingthings.things.Thing;

import java.util.ArrayList;
import java.util.List;

public class All {
    ArrayList<Thing> things;
    All(Thing... things)    {
        this.things = new ArrayList<>(List.of(things));
    }
    void update(float time)    {
        things.forEach(thing -> thing.update(time));
    }
}

package com.flopcode.movingthings;

public class MutableDouble {
    double value;
    private final String text;

    public MutableDouble(String text, double value) {
        this.value = value;
        this.text = text;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }
}

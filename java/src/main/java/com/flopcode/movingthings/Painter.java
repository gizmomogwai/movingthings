package com.flopcode.movingthings;

import com.flopcode.movingthings.things.Thing;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

class Painter extends JComponent {
    private final All all;
    private final ArrayList<Relation> relations;
    double time;
    private static final double DELTA_TIME = 0.01;
    int iterations = 1;
    BufferedImage backBuffer;

    public Painter(All all, ArrayList<Relation> relations) {
        this.all = all;
        this.relations = relations;
        setBackground(Color.BLACK);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFrame ui = new JFrame("Settings");
                ui.getContentPane().setLayout(new BorderLayout(10, 10));
                JPanel uiContent = new JPanel();
                uiContent.setLayout(new BoxLayout(uiContent, BoxLayout.Y_AXIS));
                for (Thing thing : all.things) {
                    JComponent h = thing.getGui();
                    h.setBorder(BorderFactory.createTitledBorder(thing.name));
                    uiContent.add(h);
                }
                uiContent.add(new JButton(new AbstractAction("Clear") {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        backBuffer = null;
                    }
                }));
                uiContent.add(new JButton(new AbstractAction("Reset") {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        iterations = 0;
                        backBuffer = null;
                    }
                }));
                ui.getContentPane().add(uiContent, BorderLayout.CENTER);
                ui.pack();
                ui.setVisible(true);
            }

        });
    }


    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        Graphics2D g2d = (Graphics2D) graphics;
        Dimension size = getSize();
        Point screenCenter = new Point(size.width / 2, size.height / 2);

        if (backBuffer == null || backBuffer.getWidth() != size.width || backBuffer.getHeight() != size.height) {
            backBuffer = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = prepareGraphics(backBuffer);
            g.setBackground(Color.BLACK);
            g.clearRect(0, 0, size.width, size.height);
            time = 0;
            for (int i = 0; i < iterations; ++i) {
                time += DELTA_TIME;
                all.things.forEach(thing -> thing.update(time));
                relations.forEach(relation -> relation.paint(screenCenter, g));
            }
        } else {
            Graphics2D g = prepareGraphics(backBuffer);
            time += DELTA_TIME;
            all.things.forEach(thing -> thing.update(time));
            relations.forEach(relation -> relation.paint(screenCenter, g));
        }
        graphics.drawImage(backBuffer, 0, 0, null);
        graphics.setColor(Color.white);
        all.things.forEach(thing -> thing.draw(g2d, screenCenter));
        //repaint();
        iterations++;
    }


    private Graphics2D prepareGraphics(BufferedImage backBuffer) {
        Graphics2D g = (Graphics2D) backBuffer.getGraphics();
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setStroke(new BasicStroke(1));

        g.setColor(new Color(1, 1, 1, 0.1f));
        return g;
    }
}

package com.flopcode.movingthings;

import com.flopcode.movingthings.things.Line;
import com.flopcode.movingthings.things.Planet;
import com.flopcode.movingthings.things.Sun;
import com.flopcode.movingthings.things.Thing;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {
        final JFrame root = new JFrame("MovingThings");
        root.getContentPane().setLayout(new BorderLayout());

        Thing leftRight = new Line("left-to-right", new MutablePoint("left", -200, 200), new MutablePoint("right", 200, 200), 1);
        Thing bottomTop = new Line("bottom-to-top", new MutablePoint("right", 200, 200), new MutablePoint("right", 200, -400), 1);
        Thing sun = new Sun("sun", 0, 0);
        Thing earth = new Planet("earth", sun, 300, 1, 0);
        Thing moon = new Planet("moon", earth, 50, 0.3, 0);
        Thing neptun = new Planet("neptun", sun, 400, 4.5, 0);
        //All all = new All(
//                sun, earth, moon, neptun
//        );
All all = new All(
    leftRight, bottomTop
    );
        //new Line(new Point2D.Double(400, -400), new Point2D.Double(400, 400), 1)),
        ArrayList<Relation> relations = new ArrayList<>();
        //relations.add(new Relation(earth, neptun));
//        relations.add(new Relation(moon, neptun));
        relations.add(new Relation(leftRight, bottomTop));
        root.getContentPane().add(
                new Painter(all, relations),
                BorderLayout.CENTER);
        root.setSize(800, 800);
        root.setVisible(true);

        repaint(root);
    }

    private static void repaint(JFrame root) throws InterruptedException {
        while (true) {
            Thread.sleep(18);
            SwingUtilities.invokeLater(root::repaint);
        }
    }
}

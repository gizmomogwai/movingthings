package com.flopcode.movingthings;

import com.flopcode.movingthings.things.Thing;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;

public class Relation {
    Thing a;
    Thing b;

    float hue = 0;

    public Relation(Thing a, Thing b) {
        this.a = a;
        this.b = b;
    }

    public void paint(Point screenCenter, Graphics2D g2d) {
        Color c = Color.getHSBColor(hue, 1, 1);
        hue += 0.001f;
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2f));
        g2d.setColor(c);
        Point2D p1 = a.get();
        Point2D p2 = b.get();
        g2d.drawLine((int) p1.getX() + screenCenter.x,
                (int) p1.getY() + screenCenter.y,
                (int) p2.getX() + screenCenter.x,
                (int) p2.getY() + screenCenter.y);
    }

}

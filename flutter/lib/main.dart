import 'package:flutter/material.dart';
import 'package:moving_things/things/all.dart';
import 'package:moving_things/things/line.dart';
import 'package:moving_things/things/planet.dart';
import 'package:moving_things/things/relation.dart';
import 'package:moving_things/things/sun.dart';
import 'package:moving_things/things/thing.dart';
import 'package:moving_things/widgets/animated_custom_paint.dart';
import 'package:moving_things/widgets/common.dart';
import 'package:widgetbook/widgetbook.dart';

void main() {
  runApp(const MovingThingsApp());
}

Planet planet(BuildContext context, String name, Thing parent,
    {required double initialRadius, required double initialPeriod}) {
  return Planet(
    name,
    parent,
    radius:initialRadius,
    period:initialPeriod,
  );
}

WidgetbookUseCase solarSystemRelations(String name) {
  return WidgetbookUseCase(
    name: name,
    builder: (context) {
      var common = commonKnobs(context);
      Thing sun = Sun("sun", const Offset(0.0, 0.0));
      Thing earth =
          planet(context, "earth", sun, initialRadius: 300, initialPeriod: 1);
      Thing moon =
          planet(context, "moon", earth, initialRadius: 50, initialPeriod: 0.3);
      Thing neptun = planet(context, "neptun", sun,
          initialRadius: 400, initialPeriod: 4.5);
      All all = All("solar system", [sun, earth, moon, neptun]);

      final relations = name
          .split(",")
          .map((s) => s.split("-"))
          .map((pair) => all.getRelation(pair[0], pair[1]))
          .toList(growable: false);
      for (var relation in relations) {
        relation.a.addControls(context);
        relation.b.addControls(context);
      }

      if (common.animate) {
        return AnimatedCustomPaint(
          all: all,
          relations: relations,
          common: common,
        );
      } else {
        return CustomPaint(
          willChange: true,
          isComplex: true,
          foregroundPainter: UniversePainter(
            all: all,
            relations: relations,
            common: common,
          ),
          child: Container(color: Colors.black),
        );
      }
    },
  );
}

linePairs(String name, List<dynamic> offsets) {
  return WidgetbookUseCase(
    name: name,
    builder: (context) {
      var common = commonKnobs(context);
      List<Thing> lines = [];
      int count = 0;
      for (int i = 0; i < offsets.length; i += 2) {
        count++;
        var from = offsets[i];
        var to = offsets[i + 1];
        Thing line = Line("line-$count", from, to, 1.0);
        lines.add(line);
      }
      All all = All("lines", lines);
      final List<Relation> relations = [];
      count = 0;
      for (int i = 0; i < lines.length; i += 2) {
        count++;
        relations.add(Relation("relation-$count", lines[i], lines[i + 1]));
      }
      if (common.animate) {
        return AnimatedCustomPaint(
          all: all,
          relations: relations,
          common: common,
        );
      } else {
        return CustomPaint(
          willChange: true,
          isComplex: true,
          foregroundPainter: UniversePainter(
            all: all,
            relations: relations,
            common: common,
          ),
          child: Container(color: Colors.black),
        );
      }
    },
  );
}

class MovingThingsApp extends StatelessWidget {
  const MovingThingsApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Widgetbook.material(
      appBuilder: (context, child) {
        return MaterialApp(
          title: 'Moving Things',
          home: Material(
            child: child,
          ),
        );
      },
      directories: [
        WidgetbookFolder(
          name: 'Solar System',
          children: [
            solarSystemRelations("sun-earth"),
            solarSystemRelations("earth-neptun"),
            solarSystemRelations("moon-earth"),
            solarSystemRelations("sun-earth,earth-moon"),
          ],
        ),
        WidgetbookFolder(
          name: 'Lines',
          children: [
            linePairs(
              "bottom-right",
              [
                const Offset(-200, 0),
                const Offset(0, 0),
                const Offset(0, 0),
                const Offset(0, -200),
                const Offset(0, 0),
                const Offset(0, -200),
                const Offset(200, 0),
                const Offset(0, 0),
                const Offset(-200, 0),
                const Offset(0, 0),
                const Offset(0, 0),
                const Offset(0, 200),
                const Offset(0, 0),
                const Offset(0, 200),
                const Offset(200, 0),
                const Offset(0, 0),
              ],
            ),
          ],
        ),
      ],
    );
  }
}

import 'package:flutter/widgets.dart';
import 'package:widgetbook/widgetbook.dart';

class Common {
  double zoom;
  int iterations;
  final double deltaT;
  final bool animate;
  final bool colorize;

  final bool deltaRelation;

  Common({
    required this.zoom,
    required this.iterations,
    required this.deltaT,
    required this.animate,
    required this.colorize,
    required this.deltaRelation,
  });
}

Common commonKnobs(BuildContext context) {
  return Common(
    zoom: context.knobs.double.slider(
      label: 'Zoom',
      initialValue: 1.0,
      min: 0.1,
      max: 2.0,
      divisions: 30,
    ),
    iterations: context.knobs.int.slider(
      label: 'Iterations',
      initialValue: 1000,
      min: 100,
      max: 10000,
      divisions: 100,
    ),
    deltaT: context.knobs.double.slider(
      label: 'Delta T',
      initialValue: 0.01,
      min: 0.01,
      max: 0.5,
    ),
    animate: context.knobs.boolean(label: 'Animate', initialValue: true),
    colorize: context.knobs.boolean(label: 'Colorize', initialValue: true),
    deltaRelation:
        context.knobs.boolean(label: 'Delta Relation', initialValue: false),
  );
}

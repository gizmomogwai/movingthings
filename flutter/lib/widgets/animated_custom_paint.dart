import 'package:flutter/material.dart';
import 'package:moving_things/things/all.dart';
import 'package:moving_things/things/relation.dart';
import 'package:moving_things/widgets/common.dart';

import '../things/thing.dart';

class UniversePainter extends CustomPainter {
  final All all;
  final List<Relation> relations;
  final Common common;

  UniversePainter(
      {required this.all, required this.relations, required this.common});

  Color _colorForTime(double time) {
    if (common.colorize) {
      HSVColor hsvColor = HSVColor.fromAHSV(0.2, (time * 10) % 360.0, 1.0, 1.0);
      return hsvColor.toColor();
    } else {
      return Colors.white.withOpacity(0.2);
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    Set<Thing> activeThings = {};
    canvas.translate(size.width / 2, size.height / 2);
    canvas.scale(common.zoom);
    double time = 0;
    all.update(time);
    final paint = Paint()
      ..strokeWidth = 1
      ..color = _colorForTime(time)
      ..strokeCap = StrokeCap.round;

    for (int i = 0; i < common.iterations; i++) {
      time += common.deltaT;
      paint.color = _colorForTime(time);
      all.update(time);
      for (var relation in relations) {
        relation.paint(canvas, paint, common.deltaRelation);
      }
    }
    for (var element in relations) {
      activeThings.add(element.a);
      activeThings.add(element.b);
      element.done();
    }
    for (var thing in activeThings) {
      thing.draw(canvas);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}

class AnimatedCustomPaint extends StatefulWidget {
  final All all;
  final List<Relation> relations;
  final Common common;

  const AnimatedCustomPaint(
      {super.key,
      required this.all,
      required this.relations,
      required this.common});

  @override
  State<StatefulWidget> createState() {
    return _AnimatedCustomPaintState();
  }
}

class _AnimatedCustomPaintState extends State<AnimatedCustomPaint>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<int> _iterationAnimation;

  @override
  void initState() {
    super.initState();
    var animationDuration = widget.common.iterations ~/ 150;
    _animationController = AnimationController(
        vsync: this, duration: Duration(seconds: animationDuration))
      ..forward()
      ..addListener(() {
        setState(() {});
      });
    _iterationAnimation = IntTween(begin: 0, end: widget.common.iterations)
        .animate(_animationController);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      willChange: true,
      isComplex: true,
      foregroundPainter: UniversePainter(
          all: widget.all,
          relations: widget.relations,
          common: widget.common..iterations = _iterationAnimation.value),
      child: Container(color: Colors.black),
    );
  }
}

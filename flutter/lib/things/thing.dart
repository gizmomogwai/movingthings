import 'package:flutter/material.dart' show Colors;
import 'package:flutter/painting.dart'
    show Canvas, Color, Offset, Paint, Rect, RadialGradient;
import 'package:flutter/src/widgets/framework.dart';

abstract class Thing {
  String name;
  Offset last = Offset.zero;
  List<double> fractions = [0.0, 1.0];
  List<Color> colors = [Colors.white, Colors.black];

  Thing(this.name);

  /// Calc new position
  ///
  /// @param time since start of universe
  /// @return the new position
  Offset update(double time);

  /// @return last updated position
  Offset get() {
    return last;
  }

  void draw(Canvas canvas);

  void drawPlanet(Canvas canvas, Offset pos, int radius) {
    double r2 = radius / 2;
    var leftTop = Offset(pos.dx - r2, pos.dy - r2);
    canvas.drawCircle(
        pos,
        radius.toDouble(),
        Paint()
          ..shader = RadialGradient(
            colors: colors,
            stops: fractions,
          ).createShader(
              Rect.fromCircle(center: leftTop, radius: 1.8*radius.toDouble())));
  }

  void addControls(BuildContext context);
}

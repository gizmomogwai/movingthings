import 'package:flutter/painting.dart' show Canvas, Offset, Paint;
import 'package:moving_things/things/thing.dart' show Thing;

class Relation {
  String name;
  Thing a;
  Thing b;

  Relation(this.name, this.a, this.b);

  Offset? _last;

  void paint(Canvas canvas, Paint paint, bool deltaRelation) {
    if (deltaRelation) {
      var delta = b.get() - a.get();
      if (_last != null) {
        canvas.drawLine(_last!, delta, paint);
      }
      _last = delta;
    } else {
      canvas.drawLine(a.get(), b.get(), paint);
    }
  }

  void done() {
    _last = null;
  }
}

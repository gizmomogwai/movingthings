import 'dart:ui' show Canvas, Offset;

import 'package:flutter/src/widgets/framework.dart';
import 'package:moving_things/things/thing.dart' show Thing;

class Sun extends Thing {
  Sun(String name, Offset offset) : super(name) {
    last = offset;
  }

  @override
  Offset update(double time) {
    return last;
  }

  @override
  void draw(Canvas canvas) {
    drawPlanet(canvas, last, 50);
  }

  @override
  void addControls(BuildContext context) {
    // TODO: implement addControls
  }
}

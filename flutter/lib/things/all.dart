import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:moving_things/things/relation.dart' show Relation;
import 'package:moving_things/things/thing.dart' show Thing;

class All extends Thing {
  var things = <Thing>[];

  All(String name, this.things) : super(name);

  @override
  Offset update(double time) {
    for (var thing in things) {
      thing.update(time);
    }
    return Offset.zero;
  }

  Relation getRelation(String a, String b) {
    return Relation("$a-$b", getThing(a), getThing(b));
  }

  @override
  void draw(Canvas canvas) {}

  @override
  void addControls(BuildContext context) {}

  Thing getThing(String a) {return things.where((t) => t.name == a).first;}
}


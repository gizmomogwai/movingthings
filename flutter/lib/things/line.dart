import 'dart:ui' show Canvas, Offset;

import 'package:flutter/src/widgets/framework.dart';
import 'package:moving_things/things/thing.dart' show Thing;
import 'package:widgetbook/widgetbook.dart';

class Line extends Thing {
  final Offset from;
  final Offset to;
  final double duration;
  final Offset _fromTo;
  final Offset _toFrom;
  late Offset _last;

  Line(super.name, this.from, this.to, this.duration)
      : _fromTo = (to - from),
        _toFrom = (from - to);

  @override
  Offset update(double time) {
    var current = time / duration;
    var lower = current.floor();
    var cyclus = lower as int;
    if (cyclus % 2 == 0) {
      // on the way from from to to
      _last = from + (_fromTo * (current - lower));
    } else {
      // on the way from to to from
      _last = to + (_toFrom * (current - lower));
    }
    return _last;
  }

  @override
  Offset get() {
    return _last;
  }

  @override
  void draw(Canvas canvas) {
    drawPlanet(canvas, get(), 10);
  }

  @override
  void addControls(BuildContext context) {
  }
}

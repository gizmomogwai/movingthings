import 'dart:math' show cos, pi, sin;

import 'package:flutter/widgets.dart';
import 'package:moving_things/things/thing.dart' show Thing;
import 'package:widgetbook/widgetbook.dart';

class Planet extends Thing {
  Thing parent;
  double radius;
  double period;
  double phase;

  Planet(String name, this.parent,
      {required this.radius, required this.period, this.phase = 0.0})
      : super(name);

  @override
  Offset update(double time) {
    final parent = this.parent.update(time);
    final alpha = time / period * pi * 2 + phase / 360.0 * pi * 2;
    last = Offset(
        parent.dx + radius * cos(alpha), parent.dy + radius * sin(alpha));
    return last;
  }

  @override
  void draw(Canvas canvas) {
    drawPlanet(canvas, last, 20);
  }

  @override
  void addControls(BuildContext context) {
    radius = context.knobs.double.slider(
        label: "$name-radius",
        initialValue: radius,
        min: 0,
        max: 500,
        divisions: 10);
    period = context.knobs.double.slider(
      label: "$name-period",
      initialValue: period,
      min: 0,
      max: 10,
      divisions: 20,
    );
  }
}
